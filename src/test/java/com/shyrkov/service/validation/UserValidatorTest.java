package com.shyrkov.service.validation;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.record.CreateUserRecord;
import org.junit.jupiter.api.Test;

class UserValidatorTest {

    private final UserValidator userValidator = new UserValidator();

    @Test
    void validateExceptionWhenGivenNullPassword() {
        CreateUserRecord user = new CreateUserRecord("Andrew Name", "", null);
        assertValidationExceptionWithMessage(user, "Password is missing");
    }

    @Test
    void validateExceptionWhenGivenBlankPassword() {
        CreateUserRecord user = new CreateUserRecord("Andrew Name", "", "");
        assertValidationExceptionWithMessage(user, "Password is missing");
    }

    @Test
    void validateExceptionWhenGivenTooShortPasswordPassword() {
        CreateUserRecord user = new CreateUserRecord("Andrew Name", "", "Pass");
        assertValidationExceptionWithMessage(user, "Too short password");
    }

    @Test
    void validateExceptionWhenGivenNullName() {
        CreateUserRecord user = new CreateUserRecord(null, "", "Password");
        assertValidationExceptionWithMessage(user, "User's name is missing");
    }

    @Test
    void validateExceptionWhenGivenBlankName() {
        CreateUserRecord user = new CreateUserRecord("", "", "Password");
        assertValidationExceptionWithMessage(user, "User's name is missing");
    }

    @Test
    void validateExceptionWhenGivenTooShortName() {
        CreateUserRecord user = new CreateUserRecord("a", "", "Password");
        assertValidationExceptionWithMessage(user, "Invalid user's name format");
    }

    @Test
    void validateExceptionWhenGivenIncorrectFormatName() {
        CreateUserRecord user = new CreateUserRecord("Andrew 1 N@me", "", "Password");
        assertValidationExceptionWithMessage(user, "Invalid user's name format");
    }

    @Test
    void validateCorrectUserInfoPassed() {
        CreateUserRecord user = new CreateUserRecord("Andrew Name", "Username1", "Password");
        assertDoesNotThrow(() -> userValidator.validate(user));
    }

    private void assertValidationExceptionWithMessage(CreateUserRecord user, String message) {
        ValidationException exception = assertThrows(ValidationException.class,
                                                     () -> userValidator.validate(user));
        assertEquals(message, exception.getMessage());
    }
}