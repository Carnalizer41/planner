package com.shyrkov.service.validation;

import static org.junit.jupiter.api.Assertions.*;

import com.shyrkov.web.record.CreateUserRecord;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.ValidatorFactory;
import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;

class UsernameValidatorTest {

    private final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private final Validator validator = factory.getValidator();

    @Test
    void notValidWhenUsernameBlank() {
        CreateUserRecord userRecord = new CreateUserRecord("Name", "", "Password");
        assertValidationFail(userRecord);
    }

    @Test
    void notValidWhenUsernameNull() {
        CreateUserRecord userRecord = new CreateUserRecord("Name", null, "Password");
        assertValidationFail(userRecord);
    }

    @Test
    void notValidWhenUsernameIsTooShort() {
        CreateUserRecord userRecord = new CreateUserRecord("Name", "abc", "Password");
        assertValidationFail(userRecord);
    }

    @Test
    void notValidWhenUsernameHasIncorrectFormat() {
        CreateUserRecord userRecord = new CreateUserRecord("Name", "u$er name", "Password");
        assertValidationFail(userRecord);
    }

    @Test
    void usernameValid() {
        CreateUserRecord userRecord = new CreateUserRecord("Name", "username123", "Password");
        var violations = validator.validate(userRecord);

        assertEquals(0, violations.size());
    }

    private void assertValidationFail(CreateUserRecord userRecord){
        var violations = validator.validate(userRecord);

        assertEquals(1, violations.size());

        ConstraintViolation<CreateUserRecord> violation = violations.iterator().next();
        String VALIDATION_ERROR_MESSAGE = "Invalid username: use only letters and numbers";
        assertEquals(VALIDATION_ERROR_MESSAGE, violation.getMessage());
    }
}