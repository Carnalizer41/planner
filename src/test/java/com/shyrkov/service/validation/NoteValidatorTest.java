package com.shyrkov.service.validation;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import com.shyrkov.enums.Priority;
import com.shyrkov.enums.Status;
import com.shyrkov.exceptions.AccessDeniedException;
import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.exceptions.ValueNotFoundException;
import com.shyrkov.persistence.entity.UserEntity;
import com.shyrkov.web.record.NoteRecord;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class NoteValidatorTest {

    private final NoteValidator noteValidator = new NoteValidator(null);

    @Mock
    private UserEntity user;

    @Test
    void validateExceptionWhenCommentBlank() {
        NoteRecord noteRecord = new NoteRecord(1L, null, "", "LOW", "OPENED");
        ValidationException exception = assertThrows(ValidationException.class,
                                                     () -> noteValidator.validate(noteRecord));
        assertEquals("Comment is missing", exception.getMessage());
    }

    @Test
    void validateExceptionWhenCommentNull() {
        NoteRecord noteRecord = new NoteRecord(1L, null, null, "LOW", "OPENED");
        ValidationException exception = assertThrows(ValidationException.class,
                                                     () -> noteValidator.validate(noteRecord));
        assertEquals("Comment is missing", exception.getMessage());
    }

    @Test
    void validationPassed() {
        NoteRecord noteRecord = new NoteRecord(1L, null, "Comment", "LOW", "OPENED");
        assertDoesNotThrow(() -> noteValidator.validate(noteRecord));
    }

    @Test
    void validatePriorityFound() {
        assertDoesNotThrow(() -> noteValidator.validatePriority(Priority.LOW.name()));
    }

    @Test
    void validateStatusFound() {
        assertDoesNotThrow(() -> noteValidator.validateStatus(Status.OPENED.name()));
    }

    @Test
    void validateExceptionWhenPriorityNotFound() {
        String priority = "UNKNOWN";
        ValueNotFoundException exception = assertThrows(ValueNotFoundException.class,
                                                        () -> noteValidator.validatePriority(priority));
        assertEquals(priority + " - Priority not found", exception.getMessage());
    }

    @Test
    void validateExceptionWhenStatusNotFound() {
        String status = "UNKNOWN";
        ValueNotFoundException exception = assertThrows(ValueNotFoundException.class,
                                                        () -> noteValidator.validateStatus(status));
        assertEquals(status + " - Status not found", exception.getMessage());
    }

    @Test
    void validateExceptionWhenUserHasNoAccess() {
        long ownerId = 1;
        when(user.getId()).thenReturn(2L);
        AccessDeniedException exception = assertThrows(AccessDeniedException.class,
                                                       () -> noteValidator.checkAccess(user, ownerId));
        assertEquals("You have no access to record", exception.getMessage());
    }

    @Test
    void validateUserHasAccess() {
        long ownerId = 1;
        when(user.getId()).thenReturn(1L);
        assertDoesNotThrow(() -> noteValidator.checkAccess(user, ownerId));
    }

    @Test
    void validateAdminHasAccess() {
        long ownerId = 1;
        when(user.getId()).thenReturn(2L);
        when(user.isAdmin()).thenReturn(true);
        assertDoesNotThrow(() -> noteValidator.checkAccess(user, ownerId));
    }
}