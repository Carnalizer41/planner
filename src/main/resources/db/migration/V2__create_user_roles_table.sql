CREATE TABLE IF NOT EXISTS User_roles
(
    id          SERIAL        PRIMARY KEY,
    role        VARCHAR(50)   NOT NULL,
    user_id     BIGINT        NOT NULL,
    CONSTRAINT "user_id_fkey" FOREIGN KEY (user_id) REFERENCES Users (id)
);

INSERT INTO public.Users(id, name, username, password)
VALUES (1, 'admin', 'admin', '$2a$10$sA4ts93FKtF7XU1B5qik8OazyHJY9EreEbbsNmixZ.TNmXCgZ0xAK');

INSERT INTO user_roles(role, user_id)
values ('ADMIN', 1);
