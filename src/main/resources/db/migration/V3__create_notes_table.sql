CREATE TABLE IF NOT EXISTS Notes
(
    id              SERIAL          PRIMARY KEY,
    user_id         BIGINT          NOT NULL,
    comment         VARCHAR(255)     NOT NULL,
    priority        VARCHAR(10)     NOT NULL,
    status          VARCHAR(10)     NOT NULL,
    constraint "user_id_fkey" FOREIGN KEY (user_id) REFERENCES "users" (id)
);