alter table notes
    add column version     bigint    not null default 0,
    add column last_modified timestamp not null default current_timestamp,
    add column modified_by varchar(255) not null default 'unknown';
