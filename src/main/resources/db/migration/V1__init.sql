CREATE TABLE IF NOT EXISTS Users
(
    id              SERIAL          PRIMARY KEY,
    "name"          VARCHAR(256)    NOT NULL,
    username        VARCHAR(50)     NOT NULL UNIQUE,
    password        VARCHAR(256)    NOT NULL
);