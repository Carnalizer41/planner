package com.shyrkov.service.validation;

import com.shyrkov.exceptions.NotUniqueEntityException;
import com.shyrkov.persistence.dao.UserDao;
import com.shyrkov.web.record.CreateUserRecord;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

@Component
public class NewUserValidator implements Validator<CreateUserRecord>, PasswordValidator {

    private final UserDao userDao;
    private final UserValidator userValidator;

    public NewUserValidator(UserDao userDao, UserValidator userValidator) {
        this.userDao = userDao;
        this.userValidator = userValidator;
    }

    @Override
    public void validate(@NotNull CreateUserRecord createUserRecord) {
        userValidator.validate(createUserRecord);

        if (userDao.existsByUsername(createUserRecord.username())) {
            throw new NotUniqueEntityException("User with such username already exists");
        }
    }
}
