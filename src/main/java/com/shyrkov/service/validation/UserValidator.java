package com.shyrkov.service.validation;

import static org.apache.commons.validator.GenericValidator.isBlankOrNull;
import static org.apache.commons.validator.GenericValidator.matchRegexp;
import static org.apache.commons.validator.GenericValidator.minLength;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.record.CreateUserRecord;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

@Component
public class UserValidator implements Validator<CreateUserRecord>, PasswordValidator {

    @Override
    public void validate(@NotNull CreateUserRecord user) {

        validatePassword(user.password());

        if (isBlankOrNull(user.name())) {
            throw new ValidationException("User's name is missing");
        }
        if (!minLength(user.name(), 2) || !matchRegexp(user.name(), ONLY_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid user's name format");
        }
    }
}

