package com.shyrkov.service.validation;

import static com.shyrkov.service.validation.Validator.ONLY_NUMBERS_AND_LETTERS;
import static org.apache.commons.validator.GenericValidator.isBlankOrNull;
import static org.apache.commons.validator.GenericValidator.matchRegexp;
import static org.apache.commons.validator.GenericValidator.minLength;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class UsernameValidator implements ConstraintValidator<UsernameValidation, String> {

    @Override
    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
        return !isBlankOrNull(username) &&
                minLength(username, 4) &&
                matchRegexp(username, ONLY_NUMBERS_AND_LETTERS);
    }
}
