package com.shyrkov.service.validation;

import static org.apache.commons.validator.GenericValidator.isBlankOrNull;
import static org.apache.commons.validator.GenericValidator.minLength;

import com.shyrkov.exceptions.ValidationException;

public interface PasswordValidator {

    default void validatePassword(String password) {
        if (isBlankOrNull(password)) {
            throw new ValidationException("Password is missing");
        }
        if (!minLength(password, 6)) {
            throw new ValidationException("Too short password");
        }
    }
}
