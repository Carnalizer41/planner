package com.shyrkov.service.validation;

import static org.apache.commons.validator.GenericValidator.isBlankOrNull;

import com.shyrkov.enums.Priority;
import com.shyrkov.enums.Status;
import com.shyrkov.exceptions.AccessDeniedException;
import com.shyrkov.exceptions.EntityNotFoundException;
import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.persistence.dao.NoteDao;
import com.shyrkov.persistence.entity.UserEntity;
import com.shyrkov.web.record.NoteRecord;
import org.springframework.stereotype.Component;

@Component
public class NoteValidator implements Validator<NoteRecord> {

    private final NoteDao noteDao;

    public NoteValidator(NoteDao noteDao) {
        this.noteDao = noteDao;
    }

    @Override
    public void validate(NoteRecord noteRecord) {
        validatePriority(noteRecord.priority());
        validateStatus(noteRecord.status());

        if (isBlankOrNull(noteRecord.comment())) {
            throw new ValidationException("Comment is missing");
        }

    }

    public void checkExistence(Long id) {
        if (!noteDao.existsById(id)) {
            throw new EntityNotFoundException("Note with id=" + id + " not found");
        }
    }

    public void validatePriority(String priority) {
        Priority.definePriority(priority);
    }

    public void validateStatus(String status) {
        Status.defineStatus(status);
    }

    public void checkAccess(UserEntity currentUser, Long ownerId) {
        if (!ownerId.equals(currentUser.getId()) && !currentUser.isAdmin()) {
            throw new AccessDeniedException("You have no access to record");
        }
    }
}
