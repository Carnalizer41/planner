package com.shyrkov.service.validation;

import static org.apache.commons.validator.GenericValidator.isBlankOrNull;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.record.CreateNoteRecord;
import org.springframework.stereotype.Component;

@Component
public class NewNoteValidator implements Validator<CreateNoteRecord> {

    private final NoteValidator noteValidator;

    public NewNoteValidator(NoteValidator noteValidator) {
        this.noteValidator = noteValidator;
    }

    @Override
    public void validate(CreateNoteRecord createNoteRecord) {
        if (isBlankOrNull(createNoteRecord.comment())) {
            throw new ValidationException("Comment is missing");
        }

        noteValidator.validatePriority(createNoteRecord.priority());
    }
}
