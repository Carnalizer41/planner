package com.shyrkov.service;

import com.shyrkov.enums.UserRole;
import com.shyrkov.mapper.CreateUserMapper;
import com.shyrkov.mapper.UserInfoMapper;
import com.shyrkov.persistence.dao.UserDao;
import com.shyrkov.persistence.entity.UserEntity;
import com.shyrkov.persistence.entity.UserRoleEntity;
import com.shyrkov.service.model.SimplePage;
import com.shyrkov.service.validation.NewUserValidator;
import com.shyrkov.service.validation.UserValidator;
import com.shyrkov.web.record.CreateUserRecord;
import com.shyrkov.web.record.UserInfoProjection;
import com.shyrkov.web.record.UserInfoRecord;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserService implements UserDetailsService {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final CurrentUserExtractor userExtractor;
    private final UserDao userDao;
    private final UserInfoMapper userInfoMapper;
    private final CreateUserMapper createUserMapper;
    private final UserValidator userValidator;
    private final NewUserValidator newUserValidator;

    public UserService(BCryptPasswordEncoder bCryptPasswordEncoder,
                       CurrentUserExtractor userExtractor, UserDao userDao,
                       UserInfoMapper userInfoMapper, CreateUserMapper createUserMapper,
                       UserValidator userValidator, NewUserValidator newUserValidator) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userExtractor = userExtractor;
        this.userDao = userDao;
        this.userInfoMapper = userInfoMapper;
        this.createUserMapper = createUserMapper;
        this.userValidator = userValidator;
        this.newUserValidator = newUserValidator;
    }

    public void createUser(CreateUserRecord createUserRecord) {
        newUserValidator.validate(createUserRecord);

        UserEntity userEntity = createUserMapper.recordToEntity(createUserRecord);
        userEntity.setPassword(bCryptPasswordEncoder.encode(createUserRecord.password()));

        UserRoleEntity userRole = new UserRoleEntity();
        userRole.setRole(UserRole.USER);
        userRole.setUser(userEntity);

        userEntity.setUserRoles(Collections.singletonList(userRole));

        userDao.save(userEntity);
    }

    public void updateUser(CreateUserRecord newUserRecord) {
        userValidator.validate(newUserRecord);

        UserEntity userEntity = userExtractor.getCurrentUser();
        userEntity.setName(newUserRecord.name());
        userEntity.setUsername(newUserRecord.username());
        userEntity.setPassword(bCryptPasswordEncoder.encode(newUserRecord.password()));

        userDao.updateUser(userEntity.getId(), userEntity.getUsername(), userEntity.getName(),
                           userEntity.getPassword());
    }

    public UserInfoProjection getCurrentUserInfo() {
        return getUserInfoById(userExtractor.getCurrentUser().getId());
    }

    @PreAuthorize("hasRole('ADMIN')")
    public UserInfoProjection getUserInfoById(Long id) {
        return userDao.findUserInfoById(id);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public SimplePage<UserInfoRecord> getAllUsers(Pageable pageable) {
        Page<UserEntity> page = userDao.findAll(pageable);
        return new SimplePage<>(page.getContent()
                                    .stream()
                                    .map(userInfoMapper::entityToRecord)
                                    .collect(Collectors.toList()),
                                page.getTotalElements(), pageable);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        List<GrantedAuthority> authorities = user.getUserRoles().stream().map(this::toAuthority)
                                                 .collect(Collectors.toList());

        return new User(user.getUsername(), user.getPassword(), authorities);
    }

    private GrantedAuthority toAuthority(UserRoleEntity userRoleEntity) {
        return new SimpleGrantedAuthority(userRoleEntity.getRole().name());
    }
}
