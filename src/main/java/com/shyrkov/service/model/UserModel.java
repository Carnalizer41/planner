package com.shyrkov.service.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserModel {

    private Long id;
    private String name;
    private String username;
    private String password;
    private List<UserRoleModel> roles;

    public void addRole(UserRoleModel userRole) {
        roles.add(userRole);
    }
}
