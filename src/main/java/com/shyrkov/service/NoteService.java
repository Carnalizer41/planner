package com.shyrkov.service;

import com.shyrkov.enums.Priority;
import com.shyrkov.enums.Status;
import com.shyrkov.mapper.CreateNoteMapper;
import com.shyrkov.mapper.NoteMapper;
import com.shyrkov.mapper.UserInfoMapper;
import com.shyrkov.mapper.UserMapper;
import com.shyrkov.persistence.dao.NoteDao;
import com.shyrkov.persistence.entity.NoteEntity;
import com.shyrkov.persistence.entity.UserEntity;
import com.shyrkov.service.model.SimplePage;
import com.shyrkov.service.validation.NewNoteValidator;
import com.shyrkov.service.validation.NoteValidator;
import com.shyrkov.web.record.CreateNoteRecord;
import com.shyrkov.web.record.NoteInfoRecord;
import com.shyrkov.web.record.NoteRecord;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component
public class NoteService {

    private final NoteDao noteDao;
    private final NoteMapper noteMapper;
    private final CurrentUserExtractor userExtractor;
    private final UserMapper userMapper;
    private final UserInfoMapper userInfoMapper;
    private final CreateNoteMapper createNoteMapper;
    private final NoteValidator noteValidator;
    private final NewNoteValidator newNoteValidator;

    public NoteService(NoteDao noteDao, NoteMapper noteMapper, CurrentUserExtractor userExtractor,
                       UserMapper userMapper, UserInfoMapper userInfoMapper,
                       CreateNoteMapper createNoteMapper,
                       NoteValidator noteValidator, NewNoteValidator newNoteValidator) {
        this.noteDao = noteDao;
        this.noteMapper = noteMapper;
        this.userExtractor = userExtractor;
        this.userMapper = userMapper;
        this.userInfoMapper = userInfoMapper;
        this.createNoteMapper = createNoteMapper;
        this.noteValidator = noteValidator;
        this.newNoteValidator = newNoteValidator;
    }

    public void createNote(CreateNoteRecord createNoteRecord) {
        newNoteValidator.validate(createNoteRecord);

        NoteEntity noteEntity = createNoteMapper.recordToEntity(createNoteRecord);
        noteEntity.setUserEntity(userExtractor.getCurrentUser());
        noteEntity.setStatus(Status.OPENED);

        noteDao.save(noteEntity);
    }

    public void updateNote(NoteRecord noteRecord) {
        UserEntity currentUser = userExtractor.getCurrentUser();

        noteValidator.checkAccess(currentUser, noteRecord.userRecord().id());
        noteValidator.validate(noteRecord);

        NoteEntity noteEntity = noteMapper.recordToEntity(noteRecord);
        noteEntity.setUserEntity(userMapper.recordToEntity(noteRecord.userRecord()));
        noteDao.save(noteEntity);
    }

    public NoteInfoRecord getInfoById(Long id) {
        noteValidator.checkExistence(id);
        NoteEntity noteEntity = noteDao.findNoteById(id);

        UserEntity currentUser = userExtractor.getCurrentUser();
        noteValidator.checkAccess(currentUser, noteEntity.getUserEntity().getId());

        return entityToInfo(noteEntity);
    }

    public SimplePage<NoteInfoRecord> getAll(Pageable pageable) {
        UserEntity currentUser = userExtractor.getCurrentUser();

        Page<NoteEntity> notes;
        if (!currentUser.isAdmin()) {
            notes = noteDao.findNoteEntitiesByUserEntityId(currentUser.getId(), pageable);
        } else {
            notes = noteDao.findAll(pageable);
        }

        return new SimplePage<>(notes.getContent()
                                     .stream()
                                     .map(this::entityToInfo)
                                     .collect(Collectors.toList()),
                                notes.getTotalElements(), pageable);
    }

    public SimplePage<NoteInfoRecord> applyFilter(SimplePage<NoteInfoRecord> allNotes, String filter, Object value) {
        if (value == null) {
            return allNotes;
        }

        List<NoteEntity> filtered = switch (filter) {
            case "userId" -> noteDao.findNoteEntitiesByUserEntityId((Long) value);
            case "priority" -> noteDao.findAllByPriority((Priority) value);
            case "status" -> noteDao.findAllByStatus((Status) value);
            default -> null;
        };

        List<NoteInfoRecord> filteredInfos = filtered.stream().map(this::entityToInfo).collect(
                Collectors.toList());
        List<NoteInfoRecord> intersection = (List<NoteInfoRecord>) CollectionUtils.intersection(allNotes.getContent(),
                                                                                                filteredInfos);
        return new SimplePage<>(intersection, intersection.size(), allNotes.getPageable());
    }

    private NoteInfoRecord entityToInfo(NoteEntity noteEntity) {
        return new NoteInfoRecord(userInfoMapper.entityToRecord(noteEntity.getUserEntity()),
                                  noteEntity.getComment(), noteEntity.getPriority(),
                                  noteEntity.getStatus());
    }
}
