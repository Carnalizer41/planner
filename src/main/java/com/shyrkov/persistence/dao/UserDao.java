package com.shyrkov.persistence.dao;

import com.shyrkov.persistence.entity.UserEntity;
import com.shyrkov.web.record.UserInfoProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface UserDao extends JpaRepository<UserEntity, Long> {

    UserEntity findByUsername(String username);

    boolean existsByUsername(String username);

    @Query("select u from UserEntity u where u.id = :id")
    UserInfoProjection findUserInfoById(@Param("id") Long id);

    @Transactional
    @Modifying
    @Query("""
            update UserEntity as u set
            u.name = :name, u.password = :password, u.username = :username
            where u.id = :id
            """)
    void updateUser(@Param("id") Long id, @Param("username") String username, @Param("name") String name,
                    @Param("password") String password);
}
