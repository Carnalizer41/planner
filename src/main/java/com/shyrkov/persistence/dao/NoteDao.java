package com.shyrkov.persistence.dao;

import com.shyrkov.enums.Priority;
import com.shyrkov.enums.Status;
import com.shyrkov.persistence.entity.NoteEntity;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface NoteDao extends JpaRepository<NoteEntity, Long> {

    NoteEntity findNoteById(Long id);

    Page<NoteEntity> findNoteEntitiesByUserEntityId(Long id, Pageable pageable);

    List<NoteEntity> findNoteEntitiesByUserEntityId(Long id);

    List<NoteEntity> findAllByPriority(Priority priority);

    List<NoteEntity> findAllByStatus(Status status);

    @Transactional
    @Modifying
    @Query("""
            update NoteEntity as n set
            n.comment = :comment, n.priority = :priority,
            n.status = :status where n.id = :id
            """)
    void updateNote(@Param("id") Long id, @Param("comment") String comment,
                    @Param("priority") Priority priority, @Param("status") Status status);
}
