package com.shyrkov.persistence.listeners;

import jakarta.persistence.PostPersist;
import jakarta.persistence.PrePersist;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EntityCreationListener {

    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    @PrePersist
    public void onPrePersist(Object entity) {
        log.debug(entity.getClass() + ": Pre-persist: " + formatter.format(LocalDateTime.now()));
    }

    @PostPersist
    public void onPostPersist(Object entity) {
        log.debug(entity.getClass() + ": Post-persist: " + formatter.format(LocalDateTime.now()));
    }
}