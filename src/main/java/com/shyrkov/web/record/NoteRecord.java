package com.shyrkov.web.record;

public record NoteRecord(Long id, UserRecord userRecord, String comment, String priority, String status) {
}
