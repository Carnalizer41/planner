package com.shyrkov.web.record;

public record UserRecord(Long id, String name, String username, String password) {
}
