package com.shyrkov.web.record;

public record UserInfoRecord(String name, String username) {
}
