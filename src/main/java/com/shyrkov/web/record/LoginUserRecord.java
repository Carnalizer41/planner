package com.shyrkov.web.record;

public record LoginUserRecord(String username, String password) {
}
