package com.shyrkov.web.record;

import com.shyrkov.persistence.entity.UserEntity;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "userInfoProjection", types = UserEntity.class)
public interface UserInfoProjection {
    String getName();
    String getUsername();
}
