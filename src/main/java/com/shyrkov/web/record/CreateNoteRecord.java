package com.shyrkov.web.record;

public record CreateNoteRecord(String comment, String priority) {
}
