package com.shyrkov.web.record;

import com.shyrkov.service.validation.UsernameValidation;

public record CreateUserRecord(String name, @UsernameValidation String username, String password) {
}
