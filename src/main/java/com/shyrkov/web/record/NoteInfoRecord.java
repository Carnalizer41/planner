package com.shyrkov.web.record;

import com.shyrkov.enums.Priority;
import com.shyrkov.enums.Status;

public record NoteInfoRecord(UserInfoRecord userInfo, String comment, Priority priority, Status status) {
}
