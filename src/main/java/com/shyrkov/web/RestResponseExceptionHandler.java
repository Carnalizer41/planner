package com.shyrkov.web;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import com.shyrkov.exceptions.AccessDeniedException;
import com.shyrkov.exceptions.AuthException;
import com.shyrkov.exceptions.EntityNotFoundException;
import com.shyrkov.exceptions.NotUniqueEntityException;
import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.exceptions.ValueNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = ValidationException.class)
    protected ResponseEntity<Object> handleValidationException(ValidationException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    protected ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex, WebRequest request) {
        return handleException(ex, request, FORBIDDEN);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @ExceptionHandler(value = NotUniqueEntityException.class)
    protected ResponseEntity<Object> handleNotUniqueEntityException(NotUniqueEntityException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @ExceptionHandler(value = ValueNotFoundException.class)
    protected ResponseEntity<Object> handleValueNotFoundException(ValueNotFoundException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatusCode status,
                                                                  WebRequest request) {
        List<String> list = ex.getBindingResult().getAllErrors().stream()
                              .map(DefaultMessageSourceResolvable::getDefaultMessage)
                              .collect(Collectors.toList());

        return new ResponseEntity<>(list, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = AuthException.class)
    protected ResponseEntity<Object> handleAuthException(AuthException ex, WebRequest request) {
        return handleException(ex, request, UNAUTHORIZED);
    }

    private ResponseEntity<Object> handleException(RuntimeException ex, WebRequest request, HttpStatus statusCode) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), statusCode, request);
    }
}
