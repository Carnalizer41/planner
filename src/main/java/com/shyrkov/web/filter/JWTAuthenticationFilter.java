package com.shyrkov.web.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shyrkov.enums.UserRole;
import com.shyrkov.exceptions.AuthException;
import com.shyrkov.service.model.UserModel;
import com.shyrkov.service.model.UserRoleModel;
import com.shyrkov.web.record.LoginUserRecord;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final String secret;

    public JWTAuthenticationFilter(String secret) {
        this.secret = secret;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try (ServletInputStream is = request.getInputStream()) {

            ObjectMapper objectMapper = new ObjectMapper();
            LoginUserRecord loginUserRecord = objectMapper.readValue(is, LoginUserRecord.class);

            Authentication authentication = new UsernamePasswordAuthenticationToken(loginUserRecord.username(),
                                                                                    loginUserRecord.password(),
                                                                                    Collections.emptyList());
            return getAuthenticationManager().authenticate(authentication);
        } catch (IOException e) {
            throw new AuthException(e.getMessage(), e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException {
        Object principal = authResult.getPrincipal();
        if (!(principal instanceof User user)) {
            throw new AuthException("User isn`t authenticated");
        }

        try {
            String subject = new ObjectMapper().writeValueAsString(toUserModel(user));
            Date expiresAt = new Date(System.currentTimeMillis() + 30000000);
            Algorithm algorithm = Algorithm.HMAC512(secret);
            String token = JWT.create().withSubject(subject).withExpiresAt(expiresAt).sign(algorithm);

            response.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.AUTHORIZATION);
            response.addHeader(HttpHeaders.AUTHORIZATION, token);
            response.getWriter().write("Authentication successful");

        } catch (JsonProcessingException e) {
            throw new AuthException(e.getMessage(), e);
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        super.unsuccessfulAuthentication(request, response, failed);
    }

    private UserModel toUserModel(User user) {
        UserModel result = new UserModel();
        result.setUsername(user.getUsername());
        List<UserRoleModel> roles = user.getAuthorities()
                                        .stream()
                                        .map(authority -> {
                                            UserRoleModel userRoleModel = new UserRoleModel();
                                            userRoleModel.setRole(UserRole.defineRole(authority.getAuthority()));
                                            return userRoleModel;
                                        }).collect(Collectors.toList());
        result.setRoles(roles);
        return result;
    }
}
