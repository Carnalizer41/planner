package com.shyrkov.web.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shyrkov.exceptions.AuthException;
import com.shyrkov.service.model.UserModel;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpHeaders;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final String secret;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager, String secret) {
        super(authenticationManager);
        this.secret = secret;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String headerValue = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (headerValue != null) {
            Authentication authentication = getAuthentication(headerValue);

            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(@NonNull String token) {

        String subject = JWT.require(Algorithm.HMAC512(secret))
                            .build()
                            .verify(token)
                            .getSubject();

        try {
            UserModel user = new ObjectMapper().readValue(subject, UserModel.class);
            if (user == null) {
                return null;
            }

            List<SimpleGrantedAuthority> authorities = user.getRoles().stream()
                                                           .map(role -> new SimpleGrantedAuthority(
                                                                   "ROLE_" + role.getRole()
                                                                                 .name()))
                                                           .collect(Collectors.toList());

            return new UsernamePasswordAuthenticationToken(user, null, authorities);
        } catch (JsonProcessingException e) {
            throw new AuthException(e.getMessage(), e);
        }

    }
}
