package com.shyrkov.web.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.shyrkov.config.annotation.LogExecutionTime;
import com.shyrkov.enums.Priority;
import com.shyrkov.enums.Status;
import com.shyrkov.service.NoteService;
import com.shyrkov.service.model.SimplePage;
import com.shyrkov.web.record.CreateNoteRecord;
import com.shyrkov.web.record.NoteInfoRecord;
import com.shyrkov.web.record.NoteRecord;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/notes")
public class NoteController {

    private final NoteService noteService;

    public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }

    @LogExecutionTime(additionalMessage = "Creating note")
    @Operation(security = {@SecurityRequirement(name = "api_key")})
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void createNote(@RequestBody CreateNoteRecord createNoteRecord) {
        noteService.createNote(createNoteRecord);
    }

    @LogExecutionTime(additionalMessage = "Updating note")
    @Operation(security = {@SecurityRequirement(name = "api_key")})
    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public void updateNote(@RequestBody NoteRecord noteRecord) {
        noteService.updateNote(noteRecord);
    }

    @LogExecutionTime(additionalMessage = "Retaining note")
    @Operation(security = {@SecurityRequirement(name = "api_key")})
    @GetMapping(params = "id", produces = APPLICATION_JSON_VALUE)
    public NoteInfoRecord getNoteById(@RequestParam Long id) {
        return noteService.getInfoById(id);
    }

    @LogExecutionTime(additionalMessage = "Retaining all notes")
    @Operation(security = {@SecurityRequirement(name = "api_key")})
    @GetMapping(value = "/all", produces = APPLICATION_JSON_VALUE)
    public SimplePage<NoteInfoRecord> getAllNotes(@ParameterObject @PageableDefault(size = 5) Pageable pageable,
                                                  @RequestParam(name = "userId", required = false) Long userId,
                                                  @RequestParam(name = "priority", required = false) Priority priority,
                                                  @RequestParam(name = "status", required = false) Status status) {
        SimplePage<NoteInfoRecord> allNotes = noteService.getAll(pageable);
        allNotes = noteService.applyFilter(allNotes, "userId", userId);
        allNotes = noteService.applyFilter(allNotes, "priority", priority);
        allNotes = noteService.applyFilter(allNotes, "status", status);

        return allNotes;
    }
}
