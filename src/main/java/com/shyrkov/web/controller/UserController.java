package com.shyrkov.web.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.shyrkov.config.annotation.LogExecutionTime;
import com.shyrkov.service.UserService;
import com.shyrkov.service.model.SimplePage;
import com.shyrkov.web.record.CreateUserRecord;
import com.shyrkov.web.record.UserInfoProjection;
import com.shyrkov.web.record.UserInfoRecord;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @LogExecutionTime
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void createUser(@Valid @RequestBody CreateUserRecord userRecord) {
        userService.createUser(userRecord);
    }

    @LogExecutionTime
    @Operation(security = {@SecurityRequirement(name = "api_key")})
    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public void updateUser(@Valid @RequestBody CreateUserRecord userRecord) {
        userService.updateUser(userRecord);
    }

    @LogExecutionTime
    @Operation(security = {@SecurityRequirement(name = "api_key")})
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public UserInfoProjection getCurrentUserInfo() {
        return userService.getCurrentUserInfo();
    }

    @LogExecutionTime
    @Operation(security = {@SecurityRequirement(name = "api_key")})
    @GetMapping(value = "/info", params = "id", produces = APPLICATION_JSON_VALUE)
    public UserInfoProjection getUserInfo(@RequestParam Long id) {
        return userService.getUserInfoById(id);
    }

    @LogExecutionTime
    @Operation(security = {@SecurityRequirement(name = "api_key")})
    @GetMapping(value = "/all", produces = APPLICATION_JSON_VALUE)
    public SimplePage<UserInfoRecord> getAllUsers(@ParameterObject @PageableDefault(size = 5) Pageable pageable) {
        return userService.getAllUsers(pageable);
    }
}
