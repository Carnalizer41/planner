package com.shyrkov.exceptions;

public class ValueNotFoundException extends PlannerException {

    public ValueNotFoundException(String message) {
        super(message);
    }

    public ValueNotFoundException(String message, Throwable e) {
        super(message, e);
    }
}
