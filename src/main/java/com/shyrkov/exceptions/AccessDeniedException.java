package com.shyrkov.exceptions;

public class AccessDeniedException extends PlannerException {

    public AccessDeniedException(String message) {
        super(message);
    }

    public AccessDeniedException(String message, Throwable e) {
        super(message, e);
    }
}
