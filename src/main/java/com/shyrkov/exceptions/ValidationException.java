package com.shyrkov.exceptions;

public class ValidationException extends PlannerException {

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
