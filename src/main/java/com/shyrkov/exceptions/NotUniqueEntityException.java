package com.shyrkov.exceptions;

public class NotUniqueEntityException extends PlannerException {

    public NotUniqueEntityException(String message) {
        super(message);
    }

    public NotUniqueEntityException(String message, Throwable cause) {
        super(message, cause);
    }
}
