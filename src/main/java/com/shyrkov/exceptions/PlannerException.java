package com.shyrkov.exceptions;

public class PlannerException extends RuntimeException {

    public PlannerException() {
        super();
    }

    public PlannerException(String message) {
        super(message);
    }

    public PlannerException(Throwable e) {
        super(e);
    }

    public PlannerException(String message, Throwable e) {
        super(message, e);
    }
}
