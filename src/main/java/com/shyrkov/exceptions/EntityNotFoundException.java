package com.shyrkov.exceptions;

public class EntityNotFoundException extends PlannerException {

    public EntityNotFoundException(String message) {
        super(message);
    }
}
