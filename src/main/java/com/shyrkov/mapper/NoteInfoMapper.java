package com.shyrkov.mapper;

import com.shyrkov.persistence.entity.NoteEntity;
import com.shyrkov.web.record.NoteInfoRecord;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NoteInfoMapper extends EntityToRecordMapper<NoteEntity, NoteInfoRecord>,
        RecordToEntityMapper<NoteInfoRecord, NoteEntity> {
}
