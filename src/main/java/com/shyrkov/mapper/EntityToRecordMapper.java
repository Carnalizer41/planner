package com.shyrkov.mapper;

public interface EntityToRecordMapper<E, R> {

    R entityToRecord(E entity);

}
