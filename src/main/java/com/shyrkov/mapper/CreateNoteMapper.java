package com.shyrkov.mapper;

import com.shyrkov.persistence.entity.NoteEntity;
import com.shyrkov.web.record.CreateNoteRecord;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CreateNoteMapper extends EntityToRecordMapper<NoteEntity, CreateNoteRecord>,
        RecordToEntityMapper<CreateNoteRecord, NoteEntity> {
}
