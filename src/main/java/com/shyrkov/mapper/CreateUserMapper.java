package com.shyrkov.mapper;

import com.shyrkov.persistence.entity.UserEntity;
import com.shyrkov.web.record.CreateUserRecord;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CreateUserMapper extends EntityToRecordMapper<UserEntity, CreateUserRecord>,
        RecordToEntityMapper<CreateUserRecord, UserEntity> {
}
