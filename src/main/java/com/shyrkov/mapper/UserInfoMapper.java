package com.shyrkov.mapper;

import com.shyrkov.persistence.entity.UserEntity;
import com.shyrkov.web.record.UserInfoRecord;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserInfoMapper extends EntityToRecordMapper<UserEntity, UserInfoRecord>,
        RecordToEntityMapper<UserInfoRecord, UserEntity> {
}
