package com.shyrkov.mapper;

import com.shyrkov.persistence.entity.NoteEntity;
import com.shyrkov.web.record.NoteRecord;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface NoteMapper extends EntityToRecordMapper<NoteEntity, NoteRecord>,
        RecordToEntityMapper<NoteRecord, NoteEntity> {
}
