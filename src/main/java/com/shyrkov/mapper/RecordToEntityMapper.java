package com.shyrkov.mapper;

public interface RecordToEntityMapper<R, E> {

    E recordToEntity(R record);

}
