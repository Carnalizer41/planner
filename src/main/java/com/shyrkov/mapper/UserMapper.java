package com.shyrkov.mapper;

import com.shyrkov.persistence.entity.UserEntity;
import com.shyrkov.web.record.UserRecord;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends RecordToEntityMapper<UserRecord, UserEntity>,
        EntityToRecordMapper<UserEntity, UserRecord> {
}
