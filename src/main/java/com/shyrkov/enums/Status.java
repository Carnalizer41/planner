package com.shyrkov.enums;

import com.shyrkov.exceptions.ValueNotFoundException;
import java.util.stream.Stream;

public enum Status {
    OPENED, REVIEW, CLOSED;

    public static Status defineStatus(String value) {
        return Stream.of(values())
                     .filter(v -> v.name().equals(value))
                     .findFirst()
                     .orElseThrow(() -> new ValueNotFoundException(value + " - Status not found"));
    }
}
