package com.shyrkov.enums;

import com.shyrkov.exceptions.ValueNotFoundException;
import java.util.stream.Stream;

public enum UserRole {

    ADMIN, USER;

    public static UserRole defineRole(String value) {
        return Stream.of(values())
                     .filter(v -> v.name().equals(value))
                     .findFirst()
                     .orElseThrow(() -> new ValueNotFoundException(value + " - User role not found"));
    }
}
