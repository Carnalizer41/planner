package com.shyrkov.enums;

import com.shyrkov.exceptions.ValueNotFoundException;
import java.util.stream.Stream;

public enum Priority {
    LOW, MEDIUM, HIGH, CRITICAL;

    public static Priority definePriority(String value) {
        return Stream.of(values())
                     .filter(v -> v.name().equals(value))
                     .findFirst()
                     .orElseThrow(() -> new ValueNotFoundException(value + " - Priority not found"));
    }
}
